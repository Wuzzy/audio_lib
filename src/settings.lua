local storage = minetest.get_mod_storage()

local p_settings = {}
local default_settings = {
  volume_main = 100,
  volume_bgm = 100,
  volume_sfx = 100
}


function audio_lib.load_settings(p_name)
  local settings = storage:get_string(p_name)

  if storage:get_string(p_name) == "" then
    settings = table.copy(default_settings)
    storage:set_string(p_name, minetest.serialize(settings))
    p_settings[p_name] = settings
  else
    -- TODO: fare funzione che quando vengono aggiunti/rimossi parametri (anche personalizzati), confronti e aggiorni tabella, salvando
    p_settings[p_name] = minetest.deserialize(settings)

    --v------------- legacy update, to remove in 1.0 ---------------v--
    if not p_settings[p_name].volume_sfx then
      p_settings[p_name].volume_sfx = default_settings.volume_sfx
      storage:set_string(p_name, minetest.serialize(p_settings[p_name]))
    end
    --^-------------------------------------------------------------^--
  end
end



-- se settings == "_ALL_", val deve essere tabella
function audio_lib.save_settings(p_name, setting, val)
  local old_settings = table.copy(p_settings[p_name])

  if setting ~= "_ALL_" then
    p_settings[p_name][setting] = val
  elseif type(val) == "table" then
    p_settings[p_name] = val
  end

  storage:set_string(p_name, minetest.serialize(p_settings[p_name]))
  audio_lib.reload_music(p_name, old_settings)
end





----------------------------------------------
-----------------GETTERS----------------------
----------------------------------------------

function audio_lib.get_settings(p_name)
  return p_settings[p_name]
end
