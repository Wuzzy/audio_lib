# Audio_lib

A library/API to manage audio tracks on Minetest. Pretty basic at the moment, I'm looking to introduce custom categories support and accessibility features.  


<a href="https://liberapay.com/Zughy/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support my work"/></a>  

### How To
Have a look at the [DOCS](DOCS.md)

### Commands

* `/audiosettings`: open the audio settings menu (or use the built-in item)
